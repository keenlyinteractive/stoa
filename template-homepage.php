<?php
/**
 * Template Name: Homepage Template
 */
?>
<?php 

$hero_image_id = get_post_meta($post->ID, 'hero_background_image', true);
$hero_video_id = get_post_meta($post->ID, 'hero_video', true);

$hero_image = wp_get_attachment_image_src( $hero_image_id, 'full' );
$hero_video = wp_get_attachment_image_src( $hero_video_id, 'full' );

$hero_video = wp_get_attachment_url( $hero_video_id );

$cta_title = get_post_meta($post->ID, 'cta_title', true);
$cta_button_label = get_post_meta($post->ID, 'cta_button_label', true);
$cta_button_link = get_post_meta($post->ID, 'cta_button_link', true);

$feature_title_1 = get_post_meta($post->ID, 'feature_title_1', true);
$feature_description_1 = get_post_meta($post->ID, 'feature_description_1', true);
$feature_button_link_1 = get_post_meta($post->ID, 'button_link_1', true);
$feature_button_label_1 = get_post_meta($post->ID, 'button_label_1', true);
$feature_image_id_1 = get_post_meta($post->ID, 'feature_image_1', true);
$feature_image_1 = wp_get_attachment_image( $feature_image_id_1, 'list-image' );

$feature_title_2 = get_post_meta($post->ID, 'feature_title_2', true);
$feature_description_2 = get_post_meta($post->ID, 'feature_description_2', true);
$feature_button_link_2 = get_post_meta($post->ID, 'button_link_2', true);
$feature_button_label_2 = get_post_meta($post->ID, 'button_label_2', true);
$feature_image_id_2 = get_post_meta($post->ID, 'feature_image_2', true);
$feature_image_2 = wp_get_attachment_image( $feature_image_id_2, 'list-image' );

$feature_title_3 = get_post_meta($post->ID, 'feature_title_3', true);
$feature_description_3 = get_post_meta($post->ID, 'feature_description_3', true);
$feature_button_link_3 = get_post_meta($post->ID, 'button_link_3', true);
$feature_button_label_3 = get_post_meta($post->ID, 'button_label_3', true);
$feature_image_id_3 = get_post_meta($post->ID, 'feature_image_3', true);
$feature_image_3 = wp_get_attachment_image( $feature_image_id_3, 'list-image' );

$divider_background_id = get_post_meta($post->ID, 'divider_background', true);
$divider_background = wp_get_attachment_image_src( $divider_background_id, 'full' );
$divider_title = get_post_meta($post->ID, 'divider_title', true);
$divider_content = get_post_meta($post->ID, 'divider_content', true);
?>

<section class="hero" style="background-image: url('<?php echo $hero_image[0] ?>');">
	<?php
	if ( $hero_video ) { ?>
		<div class="video-bg">
			<video autoplay loop src="<?php echo $hero_video ?>"></video>
		</div>
	<?php } ?>
	<div class="hero-overlay"></div>
	<div class="container">
		<div class="caption">
			<span>Speaking Boldly.</span>
			<span>Changing The World For Christ.</span>
		</div>
	</div>
</section>

<?php if ( $cta_title ) { ?>
	<section class="banner-cta">
		<div class="container">
			<h2><?php echo esc_html( $cta_title ); ?></h2>
			<a href="<?php echo esc_html( $cta_button_link ); ?>" class="btn btn-primary"><?php echo esc_html( $cta_button_label ); ?></a>
		</div>
	</section>
<?php } ?>

<section class="home-feature container">
	<div class="row">
		<div class="col-sm-4">
			<?php echo $feature_image_1 ?>
			<h2><?php echo esc_html( $feature_title_1 ); ?></h2>
			<p><?php echo esc_html( $feature_description_1 ); ?></p>
			<a href="<?php echo esc_html( $feature_button_link_1 ); ?>" class="btn btn-primary btn-block"><?php echo esc_html( $feature_button_label_1 ); ?></a>
		</div>
		<div class="col-sm-4">
			<?php echo $feature_image_2 ?>
			<h2><?php echo esc_html( $feature_title_2 ); ?></h2>
			<p><?php echo esc_html( $feature_description_2 ); ?></p>
			<a href="<?php echo esc_html( $feature_button_link_2 ); ?>" class="btn btn-primary btn-block"><?php echo esc_html( $feature_button_label_2 ); ?></a>
		</div>
		<div class="col-sm-4">
			<?php echo $feature_image_3 ?>
			<h2><?php echo esc_html( $feature_title_3 ); ?></h2>
			<p><?php echo esc_html( $feature_description_3 ); ?></p>
			<a href="<?php echo esc_html( $feature_button_link_3 ); ?>" class="btn btn-primary btn-block"><?php echo esc_html( $feature_button_label_3 ); ?></a>
		</div>
	</div>
</section>

<section class="divider" style="background-image: url('<?php echo $divider_background[0] ?>');">
	<div class="divider-overlay"></div>
	<div class="container">
		<h2><?php echo esc_html( $divider_title ); ?></h2>
		<p><?php echo esc_html( $divider_content ); ?></p>
	</div>
</section>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>