<header class="banner">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png" alt="Stoa Logo"></a>
    <div class="banner-right">
      <ul class="list-unstyled list-inline">
        <li class="search"><?php get_search_form(); ?><button class="open-search"><i class="fa fa-search" aria-hidden="true"></i></button></li>
        <li><a href="https://www.facebook.com/stoausa" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
        <li><a href="https://twitter.com/stoatweets" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        <li><a href="https://www.youtube.com/channel/UCxiJyCqzAjA30IUj9reIh-w/videos" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
              </ul>
      <span class="site-slogan">Christian Homeschool Speech and Debate</span>
    </div>
  </div>
  <nav class="nav-primary">
    <div class="container">
      <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png" alt="Stoa Logo"></a>
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </div>
  </nav>
  <div class="nav-spacer"></div>
  </div>
</header>
