<?php 
	// WP_Query arguments
	$args = array (
		'post_type'              => array( 'event' ),
		'orderby' => 'meta_value',
		'posts_per_page' => '3',
		'meta_key' => 'start_time',
		'order' => 'ASC',
    'meta_query' => array(
      // array(
      //   'key' => 'start_time',
      //   'value' => time(),
      //   'compare' => '>=',
      //   'type' => 'NUMERIC,'
      // ),
      array(
  			'key'	  	=> 'event_in_sidebar',
  			'value'	  	=> '1',
  			'compare' 	=> '=',
  		),
    )
	);

	// The Query
	$event = new WP_Query( $args );

	// The Loop
	if ( $event->have_posts() ) {
		while ( $event->have_posts() ) {
			$event->the_post(); 
				$event_description = get_post_meta($post->ID, 'event_description', true);
				$external_event = get_post_meta($post->ID, 'external_event', true);
				$external_event_url = get_post_meta($post->ID, 'external_event_url', true);

				if ( $external_event ) {
					$event_link = esc_url( $external_event_url );
				} else {
					$event_link = get_permalink( $post->ID );
				}
				?>

				<div class="sidebar-item">
					<h3><?php the_title(); ?></h3>
					<?php the_post_thumbnail( 'list-image' ); ?>
					<p><?php echo esc_html( $event_description ); ?></p>
					<p><a href="<?php echo $event_link; ?>" class="btn btn-primary btn-block">See Event</a></p>
				</div>
		<?php }
	}

	// Restore original Post Data
	wp_reset_postdata();
?>