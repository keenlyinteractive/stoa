<?php
if ( is_404() ) {
    $member_page = NULL;
} else {
    $member_page = get_post_meta($post->ID, 'member_page', true);
}

if ( $member_page != '1' ) { ?>
    <section class="cta">
        <div class="container">
            <h3>Your first step is connecting with a local Stoa club. We can help.</h3>
            <a class="btn btn-primary" href="http://stoausa.org/clubs/">Click Here To Connect</a>
        </div>
    </section>
<?php } ?>

<footer class="content-info">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png" alt="Stoa Logo"></a>
    <div class="contact-info">
    	<p>Stoa Christian Homeschool Speech and Debate<br>Copyright ©2016 Stoa USA<br>300 Esplanade Drive, Suite 2100 Oxnard, CA 93036</p>
    </div>
    <div class="footer-menu">
    	<!--a href="#">Terms and Conditions</a>
    	<a href="#">Privacy Policy</a-->
    </div>
  </div>
</footer>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-575afb88b4df43f7"></script>