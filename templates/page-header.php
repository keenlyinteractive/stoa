<?php use Roots\Sage\Titles; ?>
<?php 
if ( is_404() ) {
	$title_override = NULL;
} else {
	$title_override = get_post_meta($post->ID, 'page_title_override', true);
}

$title_background_id = get_post_thumbnail_id();
$title_background_custom = wp_get_attachment_image_src( $title_background_id, 'title-image' );

if ( has_post_thumbnail() ) {
	$title_background = $title_background_custom[0];
} else {
	$title_background = get_template_directory_uri() . '/dist/images/stoa-default-header.jpg';
}
?>

<div class="page-header" style="background-image: url('<?php echo $title_background ?>');">
	<div class="page-header-overlay"></div>
	<div class="container">
		<?php if ( $title_override ) { ?>
			<h1><?php echo esc_html( $title_override ); ?></h1>
		<?php } else { ?>
		  <h1><?= Titles\title(); ?></h1>
	  <?php } ?>
  </div>
</div>
