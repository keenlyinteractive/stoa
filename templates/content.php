<article <?php post_class(); ?>>
		<div class="row">
		<div class="col-sm-4">
			<?php the_post_thumbnail( 'list-image' ); ?>
		</div>
		<div class="col-sm-8">
		  <header>
		    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		    <?php get_template_part('templates/entry-meta'); ?>
		  </header>
		  <div class="entry-summary">
		    <?php the_excerpt(); ?>
		    <p><a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a></p>
		  </div>
	  </div>
  </div>
</article>