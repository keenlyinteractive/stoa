<form role="search" method="get" class="search-form" action="<?php bloginfo('url'); ?>/">
	<label>
		<span class="screen-reader-text">Search for:</span>
		<input type="search" class="search-field" placeholder="Search…"  value="<?php the_search_query(); ?>" name="s">
	</label>
	<input type="submit" class="search-submit" value="&#xf002;">
</form>