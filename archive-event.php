<div class="container">
	<?php if (!have_posts()) : ?>
	  <div class="alert alert-warning">
	    <?php _e('Sorry, no results were found.', 'sage'); ?>
	  </div>
	  <?php get_search_form(); ?>
	<?php endif; ?>
</div>
<div class="featured-event">
	<div class="container">
		<?php 
			// WP_Query arguments
			$args = array (
				'post_type'              => array( 'event' ),
				'posts_per_page'         => '1',
				'orderby' => 'meta_value',
				'meta_key' => 'start_time',
				'order' => 'ASC',
				'meta_query' => array(
            array(
                'key' => 'start_time',
                'value' => time(),
                'compare' => '>=',
                'type' => 'NUMERIC,'
                )
            ),
				'tax_query' => array(
						array(
							'taxonomy' => 'event_categories',
							'field'    => 'slug',
							'terms'    => 'featured',
						),
				),
			);

			// The Query
			$event = new WP_Query( $args );

			// The Loop
			if ( $event->have_posts() ) {
				while ( $event->have_posts() ) {
					$event->the_post(); 
						$event_description = get_post_meta($post->ID, 'event_description', true);
						$external_event = get_post_meta($post->ID, 'external_event', true);
						$external_event_url = get_post_meta($post->ID, 'external_event_url', true);
						$start_time_code = get_post_meta($post->ID, 'start_time', true);
						$end_time_code = get_post_meta($post->ID, 'end_time', true);
						$start_date = date("F j", $start_time_code);
						$end_date = date("F j", $end_time_code);

						if ( $external_event ) {
							$event_link = esc_url( $external_event_url );
						} else {
							$event_link = get_permalink( $post->ID );
						}

						if ( $start_date != $end_date ) {
							$show_end_date = ' - ' . $end_date;
						}
						?>
						<div class="event-item">
							<div class="row">
								<div class="col-sm-4">
									<?php the_post_thumbnail( 'list-image' ); ?>
								</div>
								<div class="event-content col-sm-8">
									<h2><?php the_title(); ?>, <?php echo $start_date; echo $show_end_date; ?></h2>
									<p><?php echo esc_html( $event_description ); ?></p>
									<p><a href="<?php echo $event_link; ?>" class="btn btn-primary">Details and Registration</a></p>
								</div>
							</div>
						</div>
				<?php }
			}

			// Restore original Post Data
			wp_reset_postdata();
		?>
	</div>
</div>

<div class="container">
	<p>Below is a listing of workshops provided by parties other than Stoa. The content is not provided by Stoa and thus cannot be endorsed, but the information is provided as a resource for families who wish to participate in additional training. If you have a workshop or event that you would like to list here, please <a href="<?php echo get_permalink( 19 ); ?>">contact us</a>.</p>
	<div class="main-events">

		<?php while (have_posts()) : the_post(); ?>
		  

		  <?php

		  $event_description = get_post_meta($post->ID, 'event_description', true);
		  $external_event = get_post_meta($post->ID, 'external_event', true);
		  $external_event_url = get_post_meta($post->ID, 'external_event_url', true);

		  $start_time_code = get_post_meta($post->ID, 'start_time', true);
		  $end_time_code = get_post_meta($post->ID, 'end_time', true);
		  $start_date = date("F j", $start_time_code);
		  $end_date = date("F j", $end_time_code);

		  if ( $external_event ) {
		  	$event_link = esc_url( $external_event_url );
		  } else {
		  	$event_link = get_permalink( $post->ID );
		  }

		  if ( $start_date != $end_date ) {
		  	$show_end_date = ' - ' . $end_date;
		  }
		  ?>
		  <div class="event-item">
		  	<div class="row">
		  		<div class="col-sm-4">
		  			<?php the_post_thumbnail( 'list-image' ); ?>
		  		</div>
		  		<div class="event-content col-sm-8">
		  			<h2><?php the_title(); ?>, <?php echo $start_date; echo $show_end_date; ?></h2>
		  			<p><?php echo esc_html( $event_description ); ?></p>
		  			<p><a href="<?php echo $event_link; ?>" class="btn btn-secondary">Details and Registration</a></p>
		  		</div>
		  	</div>
		  </div>

		<?php endwhile; ?>
	</div>
		

	<?php wp_bs_pagination(); ?>
</div>