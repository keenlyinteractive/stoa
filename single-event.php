<?php 
$download_1_id = get_post_meta($post->ID, 'download_1', true);
$download_1 = wp_get_attachment_url( $download_1_id );

$download_2_id = get_post_meta($post->ID, 'download_2', true);
$download_2 = wp_get_attachment_url( $download_2_id );

$download_3_id = get_post_meta($post->ID, 'download_3', true);
$download_3 = wp_get_attachment_url( $download_3_id );

$download_4_id = get_post_meta($post->ID, 'download_4', true);
$download_4 = wp_get_attachment_url( $download_4_id );

$download_5_id = get_post_meta($post->ID, 'download_5', true);
$download_5 = wp_get_attachment_url( $download_5_id );

$download_6_id = get_post_meta($post->ID, 'download_6', true);
$download_6 = wp_get_attachment_url( $download_6_id );
?>

<div class="container">
	<div class="row">
		<?php while (have_posts()) : the_post(); ?>
		  <article <?php post_class( 'main' ); ?>>
		    <div class="entry-content">
		      <?php the_content(); ?>
		    </div>
		  </article>
		  <aside class="sidebar">
		  	<div class="share">
		  		<span>Share this page</span>
					<span class="addthis_toolbox addthis_default_style addthis_32x32_style">
						<a class="addthis_button_facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
						<a class="addthis_button_twitter"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
					</span>
		  	</div>

		  	<?php

		  	// check if the repeater field has rows of data
		  	if( have_rows('documents') ): ?>

		  		<div class="event-resources">
			  		<h3>Resources for This Event</h3>

			  	 	<?php // loop through the rows of data
			  	    while ( have_rows('documents') ) : the_row(); ?>

			  	  		<?php
				  	  		$document_id = get_sub_field('document');
				  	  		$document = wp_get_attachment_url( $document_id );
			  	  		?>

			  	  		<div id="forms" class="event-resources-item">
			  	  			<span><?php the_sub_field('document_title'); ?></span><br/>
			  	  			
			  	  			
			  	  		<?php	// display a sub field value
				  	  		$file = get_sub_field('document');
				  	  		if( $file ):
				  	  	?>
        
        					<a href="<?php echo $file['url']; ?>" class="btn btn-primary" download>Download</a>
						<?php 
							endif;
							
						?>
			  	  			
			  	  			
			  	  			
			  	  		</div>

			  	    <?php endwhile; ?>

			  	    <div class="clearfix"></div>
		  	    </div>

		  	<?php else :

		  	    // no rows found

		  	endif;

		  	?>

		  </aside>
		<?php endwhile; ?>
	</div>
</div>